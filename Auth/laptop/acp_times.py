"""
=== Project 4, CIS 322, Fall 2019 ===
Author: Kayla Walker

Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import logging

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

CONTROL_RULES_OPEN = [(999, 26), (599, 28), (399, 30), (199, 32), (0, 34)]

CONTROL_RULES_CLOSE = [(999, 13.333), (599, 11.428), (0, 15)]

FINAL_BREVET_CLOSE = {1000: 75, 600: 40, 400: 27, 300: 20, 200: 13.5}


def acceptable_control_dist(control_dist_km, brevet_dist_km):
    if control_dist_km <= (brevet_dist_km * 1.2):
        return True
    return False


def calculate_time(control_dist_km, brevet_start_time, control_rules):
    logging.debug(f'entering calculate_time helper function')
    final = 0
    new_dist = control_dist_km
    for control in control_rules:
        logging.debug(f'iterating through for loop: {control}')
        low_dist, speed = control
        if new_dist > low_dist:
            logging.debug(f'entering if')
            temp = new_dist - low_dist
            logging.debug(f'temp: {temp}')
            final += temp / speed
            logging.debug(f'final: {final}')
            new_dist = low_dist

    time_string = add_hours_to_arrow(final, brevet_start_time)

    logging.debug(f'time_string: {time_string}')
    return time_string


def add_hours_to_arrow(hours, brevet_start_time):
    logging.debug(f'entering add_hours_to_arrow helper function')
    hrs = hours // 1
    mins = (hours % 1) * 60
    mins = round(mins)
    brevet_start_arrow = arrow.get(brevet_start_time)
    time_arrow = brevet_start_arrow.shift(hours=hrs, minutes=mins)
    time_string = time_arrow.isoformat()
    return time_string


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    logging.debug(f'entering open_time function')
    logging.debug(f'brevet start time: {brevet_start_time}')
    control_dist_km = round(control_dist_km)
    open_time_string = calculate_time(control_dist_km, brevet_start_time, CONTROL_RULES_OPEN)
    return open_time_string


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
        control_dist_km:  number, the control distance in kilometers
        brevet_dist_km: number, the nominal distance of the brevet
            in kilometers, which must be one of 200, 300, 400, 600, or 1000
            (the only official ACP brevet distances)
        brevet_start_time:  An ISO 8601 format date-time string indicating
            the official start time of the brevet
    Returns:
        An ISO 8601 format date string indicating the control close time.
        This will be in the same time zone as the brevet start time.
    """
    logging.debug(f'brevet start time: {brevet_start_time}')
    control_dist_km = round(control_dist_km)
    logging.debug(f'control_dist_km: {control_dist_km}')
    if control_dist_km >= brevet_dist_km:
        # override normal time calculation if control distance is above brevet distance
        final = FINAL_BREVET_CLOSE.get(brevet_dist_km)
        close_time_string = add_hours_to_arrow(final, brevet_start_time)
    elif control_dist_km < 60:
        # use French rules for control distances before 60km
        brevet_start_time = add_hours_to_arrow(1, brevet_start_time)
        close_time_string = calculate_time(control_dist_km, brevet_start_time, [(0, 20)])
    else:
        close_time_string = calculate_time(control_dist_km, brevet_start_time, CONTROL_RULES_CLOSE)
    return close_time_string
